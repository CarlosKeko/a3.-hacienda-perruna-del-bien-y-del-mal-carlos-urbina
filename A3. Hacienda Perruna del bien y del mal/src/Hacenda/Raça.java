package Hacenda;

/**
 * La classe que defineix la raça.
 * @author Carlos Urbina
 *
 */
public class Raça {
	
	private String nomRaça;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	//CONSTRUCTORS
	
	public Raça(String nom, GosMida gt, int t) {
		this (nom, gt);
		this.tempsVida = t;
		
	}

	public Raça(String nom, GosMida gt) {
		this.nomRaça = nom;
		this.mida = gt;
		this.tempsVida = 10;
		this.dominant = false;
	}
	
	//ACCESSORS
	
	/**
	 * Retorna l'atribut nomRaça de l'objecte.
	 * @return nomRaça retorna un String.
	 */
	public String getNomRaça() {
		return this.nomRaça;
	}
	
	/**
	 * Modifica l'atribut nomRaça de l'objecte.
	 * @param n rep un string.
	 */
	public void setNomRaça(String n) {
		this.nomRaça = n;
	}
	
	/**
	 * Retorna l'atribut mida de l'objecte.
	 * @return retorna un valor GosMida.
	 */
	public GosMida getMida() {
		return this.mida;
	}
	
	/**
	 * Modifica l'atribut GosMida de l'objecte.
	 * @param m rep un valor GosMida.
	 */
	public void setMida(GosMida m) {
		this.mida = m;
	}
	
	/**
	 * Retorna l'atribut tempsVida de l'objecte.
	 * @return tempsVida retorna un enter
	 */
	public int getTempsVida() {
		return this.tempsVida;
	}
	
	/**
	 * Modifica l'atribut tempsVida de l'objecte.
	 * @param tV rep un valor enter.
	 */
	public void setTempsVida(int tV) {
		this.tempsVida = tV;
	}
	
	/**
	 * Retorna l'atribut dominant de l'objecte.
	 * @return dominant retorna un boolean.
	 */
	public boolean getDominant() {
		return this.dominant;
	}
	
	/**
	 * Modifica l'atribut dominant de l'objecte. 
	 * @param d rep un valor boolean.
	 */
	public void setDominant(boolean d) {
		this.dominant = d;
	}
	
	//OPERADORS
	
	public String toString() {
		if (this.dominant) {
			return "La raça del gos és " + this.getNomRaça() + ", la mida del gos és " + this.getMida() + ", el temps de vida és de " + this.getTempsVida() + " anys i el gos és dominant";
		
		}else {
			return "La raça del gos és " + this.getNomRaça() + ", la mida del gos és " + this.getMida() + ", el temps de vida és de " + this.getTempsVida() + " anys i el gos no és dominant";
		}
	}
}

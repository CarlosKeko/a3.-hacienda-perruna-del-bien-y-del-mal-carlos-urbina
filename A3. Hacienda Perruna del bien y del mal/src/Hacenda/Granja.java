package Hacenda;

/**
 * La classe que defineix la granja.
 * @author Carlos Urbina
 *
 */

public class Granja {

	private Gos[] gossos;
	private int numGossos;
	private int topGossos;
	
	//CONSTRUCTORS
	
	/**
	 * Constructor per defecte de Granja.
	 */
	public Granja() {
		this.gossos = new Gos[100];
		this.numGossos = 0;
		this.topGossos = 100;
	}
	
	/**
	 * Constructor que rep un paràmetre enter per definir els paràmetres topGossos i gossos.
	 * @param d rep un valor enter.
	 */
	public Granja(int d) {
		if (d > 0 && d < 101) {
			this.gossos = new Gos[d];
			this.numGossos = 0;
			this.topGossos = d;
		}else {
			this.gossos = new Gos[100];
			this.numGossos = 0;
			this.topGossos = 100;
		}
	}
	
	//ACCESSORS
	
	/**
	 * Mètode que retorna l'atribut, gossos, de l'objecte.
	 * @return gossos retorna un valor Gos.
	 */
	public Gos[] getGossos() {
		return this.gossos;
	}
	
	/**
	 * Mètode que retorna l'atribut numGossos de l'objecte.
	 * @return numGossos retorna un valor enter.
	 */
	public int getNumGossos() {
		return this.numGossos;
	}
	
	/**
	 * Mètode que retorna l'atribut topGossos de l'objecte.
	 * @return numGossos retorna un valor enter.
	 */
	public int getTopGossos() {
		return this.topGossos;
	}
	
	//OPERADORS
	
	/**
	 * Mètode per afegir al vector gossos, un nou valor.
	 * @param g rep un valor Gos.
	 * @return retorna un enter.
	 */
	public int afegir(Gos g) {
		if (numGossos < topGossos && g != null) {
			this.gossos[numGossos] = g;
			this.numGossos++;
			return numGossos;
			
		}else {
			return -1;
		}
	}
	
	/**
	 * Mètode per afegir al vector gossos, un nou valor, pero aquestá vegada amb un parametre de posicio.
	 * @param g rep un valor Gos.
	 * @param n rep un valor enter.
	 */
	public void afegirAmbPosicio(Gos g, int n) {
		if (n < this.topGossos) {
			this.gossos[n] = g;
		}
	}
	
	/**
	 * Mètode que mostra les dades de la granja i la dels gossos que hi ha.
	 */
	public void visualitzar() {
		System.out.println("La granja té una capacitat per " + this.getTopGossos() + " gossos, actualment hi ha " + this.getNumGossos() + " gossos.");
		
		for (int i = 0; i < this.getTopGossos(); i++) {
			if(this.gossos[i] != null) {
				System.out.println("Gos numero " + i + ": "+ this.gossos[i].toString());
			}

		}
	}
	
	/**
	 * Mètode que mostra les dades dels gossos que estan vius.
	 */
	public void visualitzarVius() {
		for (int i = 0; i < getTopGossos(); i++) {
			if (this.gossos[i].getEstat() == GosEstat.VIU) {
				System.out.println(gossos[i].toString());
			}
		}
	}
	
	/**
	 * Mètode que mostra les dades de la granja.
	 */
	public String toString() {
		return "La granja té una capacitat per " + this.getTopGossos() + " gossos, actualment hi ha " + this.getNumGossos() + " gossos.\n";
	}
	
	/**
	 * Mètode per generar un objecte granja, també genera els gossos dins.
	 * @return novaGranja genera un objecte de tipus Granja.
	 */
	public static Granja generarGranja() {
		System.out.print("Escriu el numero de gossos que vols a la granja: ");
		int auxScanner = Test.sc.nextInt();
		Granja novaGranja = new Granja(auxScanner);
		Gos gos;
		String[] nomGossos = {"Alex", "Cameron", "Kira", "Morgan", "Robin"};
		char[] sexeGossos = {'M', 'F'};
		String[] nomRaça = {"Bóxer", "Chihuahua", "Bulldog", "Beagle", "Border collie"};
		int[] tempsVida = {10, 20, 10, 15, 17};
		GosMida[] midaGossos = {GosMida.GRAN, GosMida.MITJA, GosMida.PETIT};
		int aux;
		
		for (int i = 0; i < novaGranja.topGossos; i++) {
			aux = (int) (Math.random() * 5);
			gos = new Gos(nomRaça[aux], midaGossos[(int)(Math.random() * 3)], tempsVida[aux], (int)(Math.random() * tempsVida[aux]), nomGossos[(int)(Math.random() * 5)], (int)(Math.random() * 5), sexeGossos[(int)(Math.random() * 2)]);
			novaGranja.afegir(gos);
		}
		
		return novaGranja;
	}
	
	/**
	 * Mètode per generar un objecte granja, també genera els gossos dins. Fa el mateix que el mètode generaGranja però el genera amb un paràmetre.
	 * @return novaGranja genera un objecte de tipus Granja.
	 */
	public static Granja generarGranjaAmbParametre(int n) {
		Granja novaGranja = new Granja(n);
		Gos gos;
		String[] nomGossos = {"Alex", "Cameron", "Kira", "Morgan", "Robin"};
		char[] sexeGossos = {'M', 'F'};
		String[] nomRaça = {"Bóxer", "Chihuahua", "Bulldog", "Beagle", "Border collie"};
		int[] tempsVida = {10, 20, 10, 15, 17};
		GosMida[] midaGossos = {GosMida.GRAN, GosMida.MITJA, GosMida.PETIT};
		int aux;
		
		for (int i = 0; i < novaGranja.topGossos; i++) {
			aux = (int) (Math.random() * 5);
			gos = new Gos(nomRaça[aux], midaGossos[(int)(Math.random() * 3)], tempsVida[aux], (int)(Math.random() * tempsVida[aux]), nomGossos[(int)(Math.random() * 5)], (int)(Math.random() * 5), sexeGossos[(int)(Math.random() * 2)]);
			novaGranja.afegir(gos);
		}
		
		return novaGranja;
	}
	
	/**
	 * Mètode per obtenir la informació d'un gos en un objecte granja.
	 * @param n rep un valor enter.
	 * @return pot retornar un objecte Gos, o por retornar null.
	 */
	public Gos obtenirGos(int n) {
		if (this.gossos[n] != null) {
			return gossos[n];
			
		}else {
			return null;
		}
	}
	
}

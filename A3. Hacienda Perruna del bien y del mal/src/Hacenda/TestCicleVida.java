package Hacenda;

public class TestCicleVida {

	public static void main(String[] args) {
		
		int cicles = -1, maximGossos;
		Gos fill;
		int n = 0;
		Gos aux;
		
		while (cicles < 1 || cicles > 20) {
			try {
				System.out.print("Escriu el nombre de cicles que vols controlar: ");
				cicles = Test.sc.nextInt();
				
				if (cicles < 1 || cicles > 20) {
					System.out.println("Valor fora de rang, torna a provar.\n");
				}
				
			}catch (Exception e) {
				System.out.println("Caràcter invàlid.\n");
				Test.sc.next();
				
			}
		}
		
		Granja granja = new Granja(100);
		Gos gos1 = new Gos("Dalmata", GosMida.PETIT, 9, 2, "Carlos", 0, 'M');
		Gos gos2 = new Gos("Bóxer", GosMida.MITJA, 10, 4, "Sara", 0, 'F');
		Gos gos3 = new Gos("Chihuahua", GosMida.PETIT, 20, 6, "Alex", 0, 'F');
		Gos gos4 = new Gos("Border collie", GosMida.MITJA, 16, 2, "Julio", 0, 'M');
		Gos gos5 = new Gos("Bulldog", GosMida.GRAN, 11, 5, "Lara", 0, 'F');
		granja.afegir(gos1);
		granja.afegir(gos2);
		granja.afegir(gos3);
		granja.afegir(gos4);
		granja.afegir(gos5);
		
		for (int i = 0; i < cicles; i++) {
			maximGossos = granja.getNumGossos();
			n = 0;
			
			while (n < maximGossos){
				if (n != maximGossos - 1) {
					if (granja.obtenirGos(n) != null && granja.obtenirGos(n + 1) != null) {
						fill = granja.obtenirGos(n).aparellar(granja.obtenirGos(n + 1));
						
						if (fill != null) {
							granja.afegir(fill);
							System.out.println("Ha nascut un gosset");
							fill.visualitzar();
							granja.obtenirGos(n).setEdat(granja.obtenirGos(n).getEdat() + 1);
							granja.obtenirGos(n + 1).setEdat(granja.obtenirGos(n).getEdat() + 1);
							n = n + 2;
							
						}else {
							aux = granja.obtenirGos(n);
							granja.afegirAmbPosicio(granja.obtenirGos(n + 1), n);
							granja.afegirAmbPosicio(aux, n + 1);
							granja.obtenirGos(n).setEdat(granja.obtenirGos(n).getEdat() + 1);
							n = n + 1;
						}
					}else {
						n++;
					}
				}else {
					n++;
				}

			}

			System.out.println("-----------------------------------------------------------------------\nCicle número " + (i + 1));
			granja.visualitzar();
		}
	}

}

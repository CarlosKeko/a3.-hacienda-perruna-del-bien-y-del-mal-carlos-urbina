package Hacenda;

import java.util.Scanner;

/**
 * Episodi 1, hacenda dels gossos amorosos.
 * @author Carlos Urbina
 *
 */
public class Test {

	public static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		
		int opcio = 1;
		
		do {
			try{
				
				//Apartat 1.A.6, ens mostra la quantitat d'objectes que hem creat en el programa.
				System.out.println("\nLa quantitat de gossos creats és de: " + Gos.gossosCreats);
				
				opcio = mostrarMenu(opcio);
				
				switch (opcio) {
					case 1:
						Apartat1A1();
						pulsarEnter();
						break;
					
					case 2:
						Apartat1A3();
						pulsarEnter();
						break;
						
					case 3:
						Apartat1A4();
						pulsarEnter();
						break;
						
					case 4:
						Apartat1A5();
						pulsarEnter();
						break;
						
					case 5:
						Apartat1B3();
						pulsarEnter();
						break;
						
					case 6:
						Apartat1C();
						pulsarEnter();
						break;
					
					case 7:
						Apartat1D();
						pulsarEnter();
						break;
						
					default:
						System.out.println("\nOpció no valida");
						pulsarEnter();
						break;
				}
				
			}catch (Exception e) {
				System.out.println("\nDada invalida, torna a provar");
				pulsarEnter();

			}
			
		}while (opcio != 0);
		


	}
	
	/**
	 * Funció que s'encarrega de mostrar el menú a l'usuari, també demana un valor enter per després retornar-ho.
	 * @return opcio retorna un enter.
	 */
	public static int mostrarMenu(int opcio) {
		System.out.println("\n[1] Apartat 1.A.1");
		System.out.println("[2] Apartat 1.A.3");
		System.out.println("[3] Apartat 1.A.4");
		System.out.println("[4] Apartat 1.A.5");
		System.out.println("[5] Apartat 1.B.3");
		System.out.println("[6] Apartat 1.C");
		System.out.println("[7] Apartat 1.D");
		
		System.out.print("\nTria una opció: ");
		opcio = sc.nextInt();
		
		return opcio;
	}
	
	/**
	 * Funció per polsar enter, serveix com separador de línia entre execució i execució de processos.
	 */
	public static void pulsarEnter() {	
		String enter = "";
		System.out.print("\nPrem enter per continuar" + enter);
		sc.nextLine();
		enter = sc.nextLine();
		
	}
	
	/**
	 * Funció de l'apartat 1.A.1, crea un objecte per defecte i modifica les seves propietats amb el seu setter, després els visualitza per pantalla 
	 * utilitzant els seus respectius getters, també cridarà al mètode, borda, de la classe Gos.
	 */
	public static void Apartat1A1() {
		Gos animalPerDefecte = new Gos();
		
		animalPerDefecte.setNom("Carlos");
		System.out.println("\nEl nom del gos és " + animalPerDefecte.getNom());
		
		animalPerDefecte.setEdat(2);
		System.out.println("El gos té " + animalPerDefecte.getEdat() + " anys");
		
		animalPerDefecte.setSexe('M');
		System.out.println("El gos és de sexe " + animalPerDefecte.getSexe());
		
		animalPerDefecte.setFills(2);
		System.out.println("El gos té " + animalPerDefecte.getFills() + " fills");
		
		Gos.borda();
	}
	
	/**
	 * Funció de l'apartat 1.A.3, crea un objecte nou que es construeix amb els paràmetres que introduïm, després visualitza els seus atributs amb els
	 * mètodes visualitzar i toString de la classe Gos.
	 */
	public static void Apartat1A3() {
		Gos animal = new Gos(5, "Carlos", 0, 'M');
		animal.visualitzar();
		System.out.println(animal.toString());
	}
	
	/**
	 * Funció de l'apartat 1.A.4, crea tres objectes amb tres constructors diferents, després mostra els seus atributs per pantalla.
	 */
	public static void Apartat1A4() {
		Gos animalAmbPropietats = new Gos(1, "Paco", 0, 'M');
		Gos animalNomesNom = new Gos("Florentino");
		Gos animalCopia = new Gos(animalNomesNom);
		
		System.out.println("\n1r gos: " + animalAmbPropietats.toString());
		Gos.borda();
		
		System.out.println("\n2n gos: " + animalNomesNom.toString());
		Gos.borda();
		
		System.out.println("\n3r gos: " + animalCopia.toString());		
		Gos.borda();
	}
	
	/**
	 * Funció de l'apartat 1.A.5, que fa servir els 4 tipus de constructors que hi ha a la classe Gos i també el mètode per clonar els atributs d'un objecte.
	 */
	public static void Apartat1A5() {
		Gos animalPerDefecte = new Gos();
		Gos animalDadesIntroduides = new Gos(8, "Manuel", 7, 'M');
		Gos animalCopiat = new Gos(animalDadesIntroduides);
		Gos animalNom = new Gos("Luis");
		animalNom.clonar(animalPerDefecte);
		
		System.out.println("\n1r gos: " + animalPerDefecte.toString());
		Gos.borda();
		
		System.out.println("\n2n gos: " + animalDadesIntroduides.toString());
		Gos.borda();
		
		System.out.println("\n3r gos: " + animalCopiat.toString());		
		Gos.borda();
		
		System.out.println("\n4r gos: " + animalNom.toString());		
		Gos.borda();
		
	}
	
	/**
	 * Funció de l'apartat 1.B.3, que fa servir la classe Gos, Raça, GosEstat i GosMida per fer un objecte amb aquestes classes.
	 */
	public static void Apartat1B3() {
		Gos animalAmbRaça = new Gos("Dalmata", GosMida.GRAN, 9, 2, "Carlos", 0, 'H');
		System.out.println(animalAmbRaça.toString());
		
		Gos animalSenseRaça = new Gos();
		System.out.println(animalSenseRaça.toString());
		
		System.out.println("\nMODIFICACIÓ D'EDATS");
		
		animalAmbRaça.setEdat(11);
		System.out.println("\n" + animalAmbRaça.toString());
		
		animalSenseRaça.setEdat(11);
		System.out.println("\n" + animalSenseRaça.toString());
	}
	
	/**
	 * Funció de l'apartat 1.C, que fa servir la classe Granja per generar gossos i granjas.
	 */
	public static void Apartat1C() {
		 Granja granjaGossos = Granja.generarGranja();
		 granjaGossos.visualitzar();
		 
		 System.out.print("Escriu el número del gos que vols veure: ");
		 int aux = sc.nextInt();
		 
		 System.out.println(granjaGossos.obtenirGos(aux).toString());
		
	}
	
	/**
	* Funció de l'apartat 1.D, que fa servir totes les classes del package per generar objectes. Genera les dues primeres granges amb el mètode generarGranjaAmbParametre que fica gossos
	* generats aleatòriament a la granja.
	*/
	public static void Apartat1D() {
		Granja granja1 = Granja.generarGranjaAmbParametre(5);
		Granja granja2 = Granja.generarGranjaAmbParametre(5);
		Granja granja3 = new Granja(5);
		Gos nouGos;
		
		for (int i = 0; i < granja3.getTopGossos(); i++) {
			nouGos = granja1.obtenirGos(i).aparellar(granja2.obtenirGos(i));
			granja3.afegir(nouGos);
		}
		
		granja3.visualitzar();
	}
}

package Hacenda;

/**
 * Classe enum per posar la mida del gos.
 * @author Carlos Urbina
 *
 */
public enum GosMida {
	GRAN,
	MITJA,
	PETIT 
}
